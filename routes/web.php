<?php

use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/show-student', [StudentController::class, 'show'])->middleware('isLoggedIn');
Route::get('/delete/{id}', [StudentController::class, 'destroy']);
Route::get('/edit/{id}', [StudentController::class, 'edit'])->middleware('isLoggedIn');
Route::post('/update-form/{id}', [StudentController::class, 'update']);
Route::get('/login', [StudentController::class, 'login']);
Route::get('/register', [StudentController::class, 'register']);
Route::post('/registerStudent', [StudentController::class, 'registerStudent']);
Route::post('/loginStudent', [StudentController::class, 'loginStudent']);
Route::get('/logout', [StudentController::class, 'logout']);

