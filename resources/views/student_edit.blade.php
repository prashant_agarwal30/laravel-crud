@extends('layout');

  @section('container')
    <a class="btn btn-info" href="/show-student">back</a>
    <h1 class="d-inline">Edit Form</h1>
    
    <form method="POST" action="/update-form/{{ $student->id }}">
        @csrf
       <div class="form-group mt-5">
          <label for="username">username:</label>
          <input type="text" class="form-control" name="username" id="username" value="{{ $student->username }}" aria-describedby="helpId" placeholder="">
          <label for="roll">roll:</label>
          <input type="text" class="form-control" name="roll" id="roll" value="{{ $student->roll }}" aria-describedby="helpId" placeholder="">
          <label for="email">email:</label>
          <input type="text" class="form-control" name="email" id="email" value="{{ $student->email }}" aria-describedby="helpId" placeholder="">
          <label for="password">password:</label>
          <input type="text" class="form-control" name="password" id="password" value="{{ $student->password }}" aria-describedby="helpId" placeholder="">
          <button class="btn btn-primary mt-3" type="submit"">Update</button>
        </div>
    </form>
    @endsection
  