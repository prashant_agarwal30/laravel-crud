@extends('layout')

    @section('welcome')
      <h3 class="p-4 text-success">Welcome {{ session('username') }}</h3>
    @endsection
    
      @section('container')
        <h1 class="d-inline">Students List</h1>
        <a class="btn btn-primary" href="logout">logout</a>

        @if(Session::has('msg'))
          <div class="alert alert-info">{{ session('msg') }}</div>
        @endif

        <table class="table table-striped mt-5">
          <thead>
            <tr>
              <th scope="col">Roll</th>
              <th scope="col">Username</th>
              <th scope="col">email</th>
              <th scope="col">Created At</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody> 
              @foreach ($studentArr as $stu)
              <tr>
                  <th scope="row">{{ $stu->roll }}</th>
                  <td> {{ $stu->username }}</td>
                  <td>{{ $stu->email }}</td>
                  <td>{{ $stu->created_at }}</td>
                  @if(session('username') === 'Prashant' || session('username') === $stu->username)
                    <td><a class="btn btn-warning" href="/delete/{{ $stu->id }}">Delete</a> 
                        <a class="btn btn-info" href="/edit/{{ $stu->id }}">Update</a></td>
                  @else
                      <td></td>
                  @endif
              </tr>
              @endforeach
            </tbody>
      </table>
      {{ $studentArr->links() }}
    @endsection
