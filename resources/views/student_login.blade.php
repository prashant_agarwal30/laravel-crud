@extends('layout');

@section('container')
    <div class="row">
      <div class="col-md-4">
        <h1>Login</h1>
        <hr>

        @if(Session::has('failure'))
          <div class="alert alert-danger">
            {{ session('failure') }}
          </div>
        @endif

        <form method="POST" action="loginStudent">
            @csrf
          <div class="form-group">
            <label for="email">email:</label>
            <input type="email"
              class="form-control" name="email" id="email" value="{{ old('email') }}"  aria-describedby="helpId" placeholder="Enter Email">
              <span class="text-danger">@error('email') {{ $message }} @enderror</span>
          </div> 
          <div class="form-group">
            <label for="password">password:</label>
            <input type="password"
              class="form-control" name="password" id="password"  aria-describedby="helpId" placeholder="Enter Password">
              <span class="text-danger">@error('password') {{ $message }} @enderror</span><br>
            <button class="btn btn-primary" type="submit">login</button>
            <a href="register">Dont have a account? register here!!</a>
          </div>
        </form>
      </div>
    </div> 

@endsection
   