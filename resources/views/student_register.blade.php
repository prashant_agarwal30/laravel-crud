@extends('layout');

@section('container')
    <div class="row">
      <div class="col-md-4">
        <h1>Registration</h1>
        <hr>
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @elseif(Session::has('failure'))
            <div class="alert alert-danger">
                {{ session('failure') }}
            </div>
        @endif
        <form method="post" action="registerStudent">
            @csrf
          <div class="form-group">
          <label for="username">username:</label>
          <input type="text"
            class="form-control" name="username" id="username"  aria-describedby="helpId" placeholder="Enter Full Name">
            <span class="text-danger">@error('username') {{ $message }} @enderror</span>
          </div> 
          <div class="form-group">
            <label for="roll">roll:</label>
            <input type="text"
              class="form-control" name="roll" id="roll"  aria-describedby="helpId" placeholder="Enter Roll">
              <span class="text-danger">@error('roll') {{ $message }} @enderror</span>

          </div> 
          <div class="form-group">
            <label for="email">email:</label>
            <input type="email"
              class="form-control" name="email" id="email"  aria-describedby="helpId" placeholder="Enter Email">
              <span class="text-danger">@error('email') {{ $message }} @enderror</span>

          </div> 
          <div class="form-group">
            <label for="password">password:</label>
            <input type="password"
              class="form-control" name="password" id="password"  aria-describedby="helpId" placeholder="Enter Password">
              <span class="text-danger">@error('password') {{ $message }} @enderror</span>
              
              <button class="btn btn-primary" type="submit">register</button>
            <a href="login">Already have a account? login here!!</a>
          </div>
        </form>
      </div>
    </div> 

@endsection
    