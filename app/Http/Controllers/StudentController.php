<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class StudentController extends Controller
{
    
    public function index()
    {
        //
    }

   
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    public function show(Student $student, Request $request)
    {
        // if(! Session()->has('username')){
        //     return redirect('/login');
        // }
        $studentArr = Student::orderBy('roll', 'asc')->paginate(2);
        return view('student_show', ['studentArr'=> $studentArr]);
    }


    public function edit(Student $student, $id, Request $request)
    {
        // if(! $request->session()->has('username')){
        //     return redirect('/login');
        // }
        $student = Student::find($id);
        return view('student_edit', ['student'=> $student]);
    }


    public function update(Request $request, Student $student, $id)
    {
        $student = Student::find($id);
        $student->username = $request->username;
        $student->email = $request->email;
        $student->password = $request->password;
        $student->roll = $request->roll;
        $student->save();
        // session()->flash('msg', 'data updated successfully');
        return redirect('/show-student')->with(['msg' => 'data updated successfully']);

    }

    public function destroy(Student $student, $id)
    {
        Student::where('id',$id)->delete();
        // session()->flash('msg', 'data deleted successfully');
        return redirect('/show-student')->with(['msg' => 'data deleted successfully']);
    }

    public function login() {
       return view('student_login');
    }

    public function register() {
       return view('student_register');
    }

    public function registerStudent(Request $request) {

        $request->validate([
           'username' => 'required',
           'roll' => 'required',
           'email' => 'required|email',
           'password' => 'required'
        ]);
       $student = new Student();
       $student->username = $request->username;
       $student->roll = $request->roll;
       $student->email = $request->email;
       $student->password = Hash::make($request->password);
       $res = $student->save();
       if($res){
           return redirect('/register')->with(['success'=> ' You have registered successfully ']);
       } else {
           return redirect('/register')->with(['failure'=>' registration failed ']);
       }
    //    return redirect('/login')->with(['success', ]);
    }

    public function loginStudent(Request $request) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
         ]);    
        
         $student = Student::where('email', $request->email)->first();
         
         if($student) {
            if(Hash::check($request->password, $student->password)){
                $request->session()->put('username', $student->username);
                // echo session('username');
               return redirect('/show-student');
            } else {
                return redirect('/login')->with(['failure'=>' password does not match ']);
            }
         } else {
            return redirect('/login')->with(['failure'=>' email id does not exist ']);
         }
        
    }

    public function logout(Request $request) {
        $request->session()->flush();
        return redirect('/login');
    }
}
